// Postavljanje karte na neku početnu lokaciju
var map = L.map("map").setView({ lon: 12.4, lat: 42.4 }, 9);
const locationSpan = document.getElementById("location");

// Dodavanje funkcionalnosti za dohvat posljednih 5 korisnika koji su se ulogirali u sustav
var last_logins = document.getElementById("last-logins");
last_logins.addEventListener("click", getLastLogins);

// Dohvat trenutnog korisnika (proslijeđen kao atribut skripte)
var user = JSON.parse(document.currentScript.getAttribute("data-user"));

/* Ovdje držimo markere proteklih 5 korisnika */
var userMarkers = [];

// add the OpenStreetMap tiles
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
}).addTo(map);

// dohvaća i ispiše trenutnu lokaciju
// postavi ju i na karti
function getCurrentLocation() {
  if (!navigator.geolocation) {
    alert("Geolocation is not supported by your browser");
  } else {
    locationSpan.textContent = "Locating...";
    navigator.geolocation.getCurrentPosition(success, error);
  }

  /* Poziva se ako nije uspio dohvaćaj lokacije */
  function error(position) {
    locationSpan.textContent = "Unable to retrieve your location!";
  }

  /* Poziva se ako je uspješno dohvaćena lokacija */
  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    locationSpan.textContent =
      "Latitude: " + latitude + " Longitude: " + longitude;
    map.flyTo([latitude, longitude]); // pokaže trenutnu lokaciju na karti

    // Stavi marker i postavi mu popup
    var marker = L.marker([latitude, longitude]).addTo(map);
    var popup = "<b>User:" + user.nickname + "<br><b>Last_login:</b>" + user.updated_at;
    marker.bindPopup(popup);
    makeRequestToInsertUser(latitude, longitude);
  }
}
getCurrentLocation();

/* XMLHTTPRequest da se prilikom uspješnog lociranja ubaci u bazu */
/* POST na /profile */
function makeRequestToInsertUser(latitude, longitude) {
  var xhttp = new XMLHttpRequest();
  var url = "/profile";
  var params =
    "nickname=" +
    user.nickname +
    "&time=" +
    user.updated_at +
    "&latitude=" +
    latitude +
    "&longitude=" +
    longitude;
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      alert(xhttp.responseText);
    }
  };
  xhttp.send(params);
}

/* XmlHttpReq za dohvaćaj korisnika */
/* GET na /users */
/* Nakon što se dohvate, stavljaju se kao elementi pokraj mape i pohranjuju u mapu */
function getLastLogins() {
  var xhttp = new XMLHttpRequest();
  var url = "/users";
  xhttp.onload = function () {
    /* Inicijalno želimo da tablica, kao i karta budu prazni */
    var lastUsersDiv = document.getElementById("last-five-users");
    lastUsersDiv.innerHTML = "";
    userMarkers.forEach((element) => {
      // Brisanje markera koji su trenutno
      map.removeLayer(element);
    });
    userMarkers = [];

    /* Dohvat informacija iz xhttp requesta */
    var users = JSON.parse(xhttp.responseText);

    users.forEach((element) => {
      /* Dodavanje u listu sa strane */
      const entry = document.createElement("p");
      entry.innerHTML =
        "<b>User: </b>" + element.nickname + "<br><b>Last_login:</b>" + element.time;
      lastUsersDiv.appendChild(entry);
      

      /* Dodavanje markera i pripadajućih popupova u popis korisničkih markera */
      var marker = L.marker([
        parseFloat(element.latitude),
        parseFloat(element.longitude),
      ]);
      var popup =
        "<b>User:</b>" +
        element.nickname +
        "<br><b>Last_login:</b>" +
        element.time;
      marker.bindPopup(popup);
      userMarkers.push(marker);
      entry.addEventListener('click', () => {map.flyTo(marker.getLatLng())})
    });

    userMarkers.forEach((element) => {
      map.addLayer(element);
    });
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}
