var map = L.map("map").setView({ lon: 12.4, lat: 42.4 }, 9);
const locationSpan = document.getElementById("location");

// add the OpenStreetMap tiles
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
}).addTo(map);

function getCurrentLocation() {
  if (!navigator.geolocation) {
    alert("Geolocation is not supported by your browser");
  } else {
    locationSpan.textContent = "Locating...";
    navigator.geolocation.getCurrentPosition(success, error);
  }

  /* Poziva se ako nije uspio dohvaćaj lokacije */
  function error(position) {
    locationSpan.textContent = "Unable to retrieve your location!";
  }

  /* Poziva se ako je uspješno dohvaćena lokacija */
  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    locationSpan.textContent =
      "Latitude: " + latitude + " Longitude: " + longitude;
    map.flyTo([latitude, longitude]); // pokaže trenutnu lokaciju na karti

    // Stavi marker
    var marker = L.marker([latitude, longitude]).addTo(map);
  }
}
getCurrentLocation();
