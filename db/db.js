const sqlite3 = require("sqlite3").verbose();
let db;

function openConnection() {
  (db = new sqlite3.Database(":memory:")),
    (err) => {
      if (err) {
        return console.log("err1");
      }
      console.log("Connected to the in-memory SQlite DB");
    };
}

function closeConnection() {
  db.close((err) => {
    if (err) return console.log(err.message);
    else console.log("Closing connection to the in-memory SQlite DB");
  });
}

function init() {
  openConnection();
  console.log("DB_INIT()");
  db.run(
    "CREATE TABLE users(nickname TEXT , time TEXT UNIQUE, latitude REAL, longitude REAL)"
  );
}

function insertUser(nickname, time, latitude, longitude) {
  db.run(
    "INSERT INTO users(nickname,time,latitude,longitude) VALUES (?,?,?,?)",
    [nickname, time, latitude, longitude],
    function (err) {
      if (err) {
        return console.log(err.message);
      }
      console.log("A row has been inserted: " + nickname + " " + time);
    }
  );
}

async function getLoginUsers() {
  const res = await new Promise((resolve, reject) => {
    db.all(
      "SELECT * FROM users ORDER BY datetime(time) DESC LIMIT 5",
      [],
      function (err, rows) {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.stringify(rows));
        }
      }
    );
  });
  return res;
}

module.exports = { init, closeConnection, insertUser, getLoginUsers };
