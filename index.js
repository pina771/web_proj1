const express = require("express");
const app = express();
const { auth, requiresAuth } = require("express-openid-connect");
require("dotenv").config();
const db = require("./db/db");

const port = process.env.PORT || 3000;

/* Setup za Auth0 */
app.use(
  auth({
    authRequired: false,
    auth0Logout: true,
    issuerBaseURL: process.env.ISSUER_BASE_URL,
    baseURL: process.env.BASE_URL,
    clientID: process.env.CLIENT_ID,
    secret: process.env.SECRET,
    idpLogout: true,
  })
);

/* Za parsiranje requesta */
app.use(express.urlencoded());

/* Inicijalizacija SQlite memory baze podataka */
db.init();
/* Za serviranje statičkog sadržaja i skripta, da možemo referencirati u views-ima */
app.use(express.static(__dirname + "/assets"));

/* EJS - chosen view engine */
app.set("view engine", "ejs");

app.get("/", (req, res, next) => {
  if (req.oidc.isAuthenticated()) res.redirect("/profile");
  else res.render("home");
});

app.get("/profile", requiresAuth(), (req, res, next) => {
  res.render("profile", { user: req.oidc.user });
});

/* Prilikom zatraženja lokacije, šalje se xmlhttp(POST)-request sa informacijama */
app.post("/profile", requiresAuth(), (req, res, next) => {
  console.log("POST REQUEST succesfully made : " + req.body.nickname);
  db.insertUser(
    req.body.nickname,
    req.body.time,
    req.body.latitude,
    req.body.longitude
  );
  res.status(200);
});

/* Prilikom zatraženja lokacije i informacija o posljednjih 5 korisnika, šalje se xmlhttp(GET)-request */
/*  */
app.get("/users", requiresAuth(), (req, res, next) => {
  (async() => {
    var selectResults = await db.getLoginUsers();
    res.status(200).send(selectResults);
  })();
});

const server = app.listen(port, () => {
  console.log("Listening on port " + port);
});

/* Graceful termination / exit */
process.on("SIGTERM", () => {
  db.closeConnection();
  server.close(() => {
    console.log("Process terminated, closing server");
  });
});
process.on("SIGINT", () => {
  db.closeConnection();
  server.close(() => {
    console.log("Process terminated, closing server");
  });
});
